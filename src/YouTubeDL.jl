module YouTubeDL

using PythonCall, CondaPkg
using URIs, Dates, Term

using Base: RefValue


const ytdl_dateformat = dateformat"yyyymmdd"

const py_ytdl = PythonCall.pynew()

__init__() = PythonCall.pycopy!(py_ytdl, pyimport("youtube_dl"))


struct Connection
    py::Py
end

function Connection(;quiet::Bool=true, kw...)
    params = pydict((;quiet, kw...))
    Connection(py_ytdl.YoutubeDL(;params))
end

function raw_getinfo(χ::Connection, url::AbstractString; download::Bool=false)
    χ.py.extract_info(url; download)
end

function getinfo(χ::Connection, url::AbstractString; kw...)
    pyconvert(Dict{String,Any}, raw_getinfo(χ, url; kw...))
end


struct Video
    id::String
    title::String
    channel::String
    uploader::String
    uploader_id::String
    duration::Float64  # in seconds
    fps::Float64
    width::Int
    height::Int
    upload_date::Date
    description::String
    extractor::String
    thumbnail_url::URI
    webpage_url::URI
    file_ext::String  # this does not depend on specific file
    file::RefValue{String}
end

#TODO: need to figure out what to do if we started downloading already
#... might need defaults for file name and stuff
function Video(info::Py)
    Video(pyconvert(String, info["id"]),
          pyconvert(String, info["title"]),
          pyconvert(String, info["channel"]),
          pyconvert(String, info["uploader"]),
          pyconvert(String, info["uploader_id"]),
          pyconvert(Float64, info["duration"]),
          pyconvert(Float64, info["fps"]),
          pyconvert(Int, info["width"]),
          pyconvert(Int, info["height"]),
          Date(pyconvert(String, info["upload_date"]), ytdl_dateformat),
          pyconvert(String, info["description"]),
          pyconvert(String, info["extractor"]),
          URI(pyconvert(String, info["thumbnail"])),
          URI(pyconvert(String, info["webpage_url"])),
          pyconvert(String, info["ext"]),
          RefValue{String}(),
         )
end
Video(χ::Connection, url::AbstractString; download::Bool=false, kw...) = Video(raw_getinfo(χ, url; download, kw...))

end
