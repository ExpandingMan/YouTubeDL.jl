using YouTubeDL
using Documenter

DocMeta.setdocmeta!(YouTubeDL, :DocTestSetup, :(using YouTubeDL); recursive=true)

makedocs(;
    modules=[YouTubeDL],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/YouTubeDL.jl/blob/{commit}{path}#{line}",
    sitename="YouTubeDL.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/YouTubeDL.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
