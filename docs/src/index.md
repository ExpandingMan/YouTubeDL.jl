```@meta
CurrentModule = YouTubeDL
```

# YouTubeDL

Documentation for [YouTubeDL](https://gitlab.com/ExpandingMan/YouTubeDL.jl).

```@index
```

```@autodocs
Modules = [YouTubeDL]
```
