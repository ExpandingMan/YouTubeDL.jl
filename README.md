# YouTubeDL

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/YouTubeDL.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/YouTubeDL.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/YouTubeDL.jl/-/pipelines)


A Julia wrapper of [`youtube-dl`](https://github.com/ytdl-org/youtube-dl).  `youtube-dl` is designed
primarily as a command line application but we do our best to make it into a useful library without
relying on too many internals.

It is explicitly a non-goal of this package to reproduce core functionality of `youtube-dl` in
Julia.  Maintaining that package is a nightmare and the maintainers are subject to the whims of the
google web designers.  This package is intended to work as long as `youtube-dl` does and not to
require separate maintenance.
